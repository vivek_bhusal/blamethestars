/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.btshome;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int aries=0x7f020000;
        public static final int check=0x7f020001;
        public static final int ic_launcher=0x7f020002;
        public static final int question=0x7f020003;
        public static final int stars=0x7f020004;
    }
    public static final class id {
        public static final int Zordiac_sign=0x7f08002c;
        public static final int action_settings=0x7f08002e;
        public static final int aquarius_menu=0x7f08001d;
        public static final int aries_menu=0x7f080013;
        public static final int ask_a_question=0x7f080011;
        public static final int birth_place=0x7f08002a;
        public static final int cancer_menu=0x7f080016;
        public static final int capricon_menu=0x7f08001c;
        public static final int dateofbirth=0x7f080028;
        public static final int dialogButtonOK=0x7f08000a;
        public static final int email=0x7f080025;
        public static final int first_name=0x7f080023;
        public static final int fun_fact=0x7f080000;
        public static final int gemini_menu=0x7f080015;
        public static final int hor_nm=0x7f080001;
        public static final int horoscope_description=0x7f080005;
        public static final int horoscope_icon=0x7f080004;
        public static final int horoscope_intro=0x7f080002;
        public static final int horoscope_month=0x7f080003;
        public static final int lastname=0x7f080024;
        public static final int leo_menu=0x7f080017;
        public static final int libra_menu=0x7f080019;
        public static final int life_preditcion=0x7f080012;
        public static final int log_in_button=0x7f080007;
        public static final int login_fun_fact=0x7f08000b;
        public static final int login_horoscope_description=0x7f080010;
        public static final int login_horoscope_icon=0x7f08000f;
        public static final int login_horoscope_month=0x7f08000e;
        public static final int more_horo=0x7f080006;
        public static final int password=0x7f080026;
        public static final int phone=0x7f080027;
        public static final int pieces_menu=0x7f08001e;
        public static final int sagittarius_menu=0x7f08001b;
        public static final int scorpio_menu=0x7f08001a;
        public static final int sex=0x7f08002b;
        public static final int taurus_menu=0x7f080014;
        public static final int text=0x7f080009;
        public static final int timeofbirth=0x7f080029;
        public static final int user_email=0x7f08001f;
        public static final int user_password=0x7f080020;
        public static final int user_question=0x7f080008;
        public static final int user_sign_up=0x7f080022;
        public static final int user_signup=0x7f08002d;
        public static final int user_submit=0x7f080021;
        public static final int virgo_menu=0x7f080018;
        public static final int welcome_salutation=0x7f08000c;
        public static final int welcome_username=0x7f08000d;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int ask_question=0x7f030001;
        public static final int custom=0x7f030002;
        public static final int login_page=0x7f030003;
        public static final int menu=0x7f030004;
        public static final int sign_in=0x7f030005;
        public static final int sign_up=0x7f030006;
    }
    public static final class menu {
        public static final int main=0x7f070000;
    }
    public static final class string {
        public static final int action_settings=0x7f050001;
        public static final int app_name=0x7f050000;
        public static final int ask_a_question=0x7f05000e;
        public static final int button_sign_up=0x7f05002c;
        public static final int did_you_know=0x7f050003;
        public static final int email=0x7f05001e;
        public static final int hav_sign_up=0x7f050020;
        public static final int hello_world=0x7f050002;
        public static final int horoscope=0x7f050005;
        public static final int horoscope_description=0x7f050009;
        public static final int horoscope_menu_item=0x7f050010;
        public static final int horoscope_month=0x7f050008;
        public static final int horoscope_sign=0x7f050007;
        public static final int life_prediction=0x7f05000f;
        public static final int login_button=0x7f05000d;
        public static final int login_welcome=0x7f05000a;
        public static final int login_welcome_name=0x7f05000b;
        public static final int menu_aquarius=0x7f05001b;
        public static final int menu_aries=0x7f050011;
        public static final int menu_cancer=0x7f050014;
        public static final int menu_capricon=0x7f05001a;
        public static final int menu_gemini=0x7f050013;
        public static final int menu_leo=0x7f050015;
        public static final int menu_libra=0x7f050017;
        public static final int menu_pieces=0x7f05001c;
        public static final int menu_sagittarius=0x7f050019;
        public static final int menu_scorpio=0x7f050018;
        public static final int menu_taurus=0x7f050012;
        public static final int menu_virgo=0x7f050016;
        public static final int more_horo=0x7f05000c;
        public static final int password=0x7f05001f;
        public static final int sign_in=0x7f05001d;
        public static final int sign_up=0x7f050021;
        public static final int this_month=0x7f050006;
        public static final int user_date_of_birth=0x7f050029;
        public static final int user_email_id=0x7f050024;
        public static final int user_first_name=0x7f050022;
        public static final int user_horoscope_id=0x7f050028;
        public static final int user_last_name=0x7f050023;
        public static final int user_password=0x7f050026;
        public static final int user_phone_number=0x7f050025;
        public static final int user_place_of_birth=0x7f05002b;
        public static final int user_sex=0x7f050027;
        public static final int user_time_of_birth=0x7f05002a;
        public static final int you_know_fill=0x7f050004;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
