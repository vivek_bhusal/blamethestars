package com.example.btshome;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.btshome.library.SharePrefManager;
import com.example.btshome.library.UserFunctions;
import com.example.btshome.library.checkconnection;

public class SignUp extends Activity{

	JSONObject jObj;
	String role = "2";
	 
	EditText firstname, lastname, email, password, phoneno, dateofbirth, timeofbirth, placeofbirth, sex, zordiac;
	String Strfname, STRlname, STRemail, STRpassword, STRphoneno, STRdateofbirth, STRtimeofbirth, STRplaceofbirth, STRsex, STRzordic;
	Button signup;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sign_up);
		
		firstname = (EditText) findViewById(R.id.first_name);
		lastname = (EditText) findViewById(R.id.lastname);
		email = (EditText) findViewById(R.id.email);
		password = (EditText) findViewById(R.id.password);
		phoneno = (EditText) findViewById(R.id.phone);
		dateofbirth = (EditText) findViewById(R.id.dateofbirth);
		timeofbirth = (EditText) findViewById(R.id.timeofbirth);
		placeofbirth = (EditText) findViewById(R.id.birth_place);
		sex = (EditText) findViewById(R.id.sex);
		zordiac = (EditText) findViewById(R.id.Zordiac_sign);
		
		signup = (Button) findViewById(R.id.user_signup);
		signup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Strfname = firstname.getText().toString();
				STRlname = lastname.getText().toString();
				STRemail = email.getText().toString();
				STRpassword = password.getText().toString();
				STRphoneno = phoneno.getText().toString();
				STRdateofbirth = dateofbirth.getText().toString();
				STRtimeofbirth = timeofbirth.getText().toString();
				STRplaceofbirth = placeofbirth.getText().toString();
				STRsex = sex.getText().toString();
				STRzordic = zordiac.getText().toString();
				if(Strfname.length()<1 || STRlname.length()<1 || STRemail.length()<1 || STRpassword.length()<1 || STRphoneno.length()<1 || STRdateofbirth.length() <1
						|| STRtimeofbirth.length()<1 || STRplaceofbirth.length()<1 || STRsex.length()<1 || STRzordic.length()<1 ){
					Toast.makeText(getApplicationContext(), "Data on some fields are mission", Toast.LENGTH_LONG).show();
				}else{
					if(checkconnection.checkInternetConnection(SignUp.this)){
						Asynconnection2 connection = new Asynconnection2();
						connection.execute();
					}
				}
			}
		});
	}
	private class Asynconnection2 extends AsyncTask<Void, Void, Void>{

		JSONObject jsob;
		ProgressDialog progress;
		
		@Override
		protected void onPreExecute() {
			progress = new ProgressDialog(SignUp.this);
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setMessage("Processing Request ...");
			progress.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			UserFunctions user = new UserFunctions();
			jsob = user.registerURL(Strfname, STRlname, STRemail, STRpassword, STRphoneno, STRsex, role, STRdateofbirth, STRzordic, STRtimeofbirth, STRplaceofbirth);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			progress.dismiss();
			try {
				if(jsob.getString("status").equalsIgnoreCase("success")){
					JSONObject dataobj = jsob.getJSONObject("data");
					JSONObject userobj = dataobj.getJSONObject("user");
					JSONObject profileobj = dataobj.getJSONObject("profile");
					
					String firstname = userobj.getString("firstname");
					String userid = userobj.getString("iduser");
					String horoscope_id = profileobj.getString("horoscope_id");
					
					SharePrefManager SM = new SharePrefManager(SignUp.this);
					SM.putuserDetails("1", userid, firstname, horoscope_id);
					
					Intent intent = new Intent(SignUp.this, LoginPage.class);
					startActivity(intent);
					Toast.makeText(getApplicationContext(),jsob.getString("message") , Toast.LENGTH_LONG).show();
					
				}else{
					Toast.makeText(getApplicationContext(),jsob.getString("message") , Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

}
