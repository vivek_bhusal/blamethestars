package com.example.btshome;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.btshome.library.SharePrefManager;
import com.example.btshome.library.UserFunctions;

public class LoginActivity extends Activity{
	
	EditText username, password;
	String user, pass;
	Button submit,sign_up;
	Dialog dialog;
	JSONObject jObj;
	private static final String STATUS = "status";
	private static final String MESSAGE = "message";
	final Context context = this;
	int request_Code = 1;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);
        username = (EditText)findViewById(R.id.user_email);
        password = (EditText)findViewById(R.id.user_password);
        submit = (Button)findViewById(R.id.user_submit);
        
        
        submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				user = username.getText().toString();
		        pass = password.getText().toString();
		        if(user.length()<1 || pass.length()<1){
		        	Toast.makeText(getApplicationContext(), "fill user name and pass", Toast.LENGTH_SHORT).show();
		        }else{
		        	
		        	new Asynconnection().execute();
		        	
		       }
			}
		});
        
        sign_up = (Button)findViewById(R.id.user_sign_up);
        sign_up.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			Intent i = new Intent(LoginActivity.this, SignUp.class);
			startActivity(i);
			}
		});
	}   
	
	public class Asynconnection extends AsyncTask<Void, Void, Void>{
		
		ProgressDialog progress;
		
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			progress = new ProgressDialog(LoginActivity.this);
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setMessage("Processing Request ...");
			progress.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			UserFunctions users = new UserFunctions();
			jObj = users.login(user, pass);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			progress.dismiss();
			try{
				String status = jObj.getString(STATUS);
				if (status.equalsIgnoreCase("success"))
				{
					JSONObject dataobj = jObj.getJSONObject("data");

					String firstname = dataobj.getString("firstname");
					String userid =dataobj.getString("iduser");
					String horoscope_id = dataobj.getString("horoscope_id");
					
					SharePrefManager SM = new SharePrefManager(LoginActivity.this);
					SM.putuserDetails("1", userid, firstname, horoscope_id);
					
					Intent intent = new Intent(LoginActivity.this, LoginPage.class);
					startActivity(intent);
										
				}
				else{
					Toast.makeText(getApplicationContext(),jObj.getString(MESSAGE) , Toast.LENGTH_LONG).show();
				}
			}catch(JSONException e)
			{
				e.printStackTrace();
			}
			
			super.onPostExecute(result);
		}
		
		
		
	}
}	

