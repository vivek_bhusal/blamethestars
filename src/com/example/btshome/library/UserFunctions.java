package com.example.btshome.library;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;


public class UserFunctions {
	
	private JSONParser jsonParser;
	private static String loginURL = "http://dev.yipl.com.np/blamethestars/public/api/user/login";
	private static String registerURL = "http://dev.yipl.com.np/blamethestars/public/api/user/register";
	

//	private static String loginURL = "http://colorstoweb.com/uber/interface/v10/userlogin";
	private static int PUT = 1;
	private static int GET = 2;
	private static int POST = 3;
	private JSONObject json= null;
	
	// constructor
	public UserFunctions(){
		jsonParser = new JSONParser();
	}
	
	public JSONObject login(String username, String password){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("email", username));
		params.add(new BasicNameValuePair("password", password));
		try {
			json = jsonParser.getJSONFromUrl(loginURL, params, POST);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		return json;
	}

	
	public JSONObject registerURL(String fname,String lname,String mail,String password,String number,String sex,String role,String dob,String horoscope,String tob,String pob){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("first_name", fname));
		params.add(new BasicNameValuePair("last_name", lname));
		params.add(new BasicNameValuePair("email",mail));
		params.add(new BasicNameValuePair("phone",number));
		params.add(new BasicNameValuePair("password",password));
		params.add(new BasicNameValuePair("sex",sex));
		params.add(new BasicNameValuePair("role", role));
		params.add(new BasicNameValuePair("date_of_birth",dob));
		params.add(new BasicNameValuePair("horoscope_id",horoscope));
		params.add(new BasicNameValuePair("time_of_birth",tob));
		params.add(new BasicNameValuePair("place_of_birth",pob));
		try {
			json = jsonParser.getJSONFromUrl(registerURL, params, POST);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public JSONObject allZodiacs(){
		try {
			json = jsonParser.getJSONFromUrl(registerURL, null, GET);
		} catch (URISyntaxException e){ 
			e.printStackTrace();
		}
		return json;
	}

}
