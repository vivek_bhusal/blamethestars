package com.example.btshome.library;
import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
 
public class SharePrefManager {
    // Shared Preferences
    SharedPreferences pref;
 
    // Editor for Shared preferences
    Editor editor;
 
    // Context
    Context _context;
 
    // Shared pref mode
    int PRIVATE_MODE = 0;
 
    // Sharedpref file name
    private static final String PREF_NAME = "storesPref";
 
    public static final String KEY_ISLOGEDIN = "islogin";
 
    // User name (make variable public to access from outside)
    public static final String KEY_cid = "cid";
    public static final String KEY_name = "name";
    public static final String KEY_horoscope_id = "hid";
 
 
    // Constructor
    public SharePrefManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
 
    /**
     * Create login session
     * */
    public void putuserDetails(String islogin, String cid, String name, String hid){
        // Storing name in pref
        editor.putString(KEY_ISLOGEDIN, islogin);
        editor.putString(KEY_cid, cid);
        editor.putString(KEY_name, name);
        editor.putString(KEY_horoscope_id, hid);
        editor.commit();
    }   
 
    public HashMap<String, String> getuserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_ISLOGEDIN, pref.getString(KEY_ISLOGEDIN, "0"));
        user.put(KEY_cid, pref.getString(KEY_cid, null));
        user.put(KEY_name, pref.getString(KEY_name, null));
        user.put(KEY_horoscope_id, pref.getString(KEY_horoscope_id, null));
 
        return user;
    }
 
}