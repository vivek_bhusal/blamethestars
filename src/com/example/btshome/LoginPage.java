package com.example.btshome;

import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.btshome.library.SharePrefManager;
import com.example.btshome.library.UserFunctions;

public class LoginPage extends Activity{
	
	JSONObject jObj;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_page);
		
		SharePrefManager SM = new SharePrefManager(LoginPage.this);
		HashMap<String, String> userdetails = new HashMap<String, String>();
		 userdetails = SM.getuserDetails();
		 
		 String username = userdetails.get(SharePrefManager.KEY_name);
		 
		
		//---get the EditText view---
		TextView userName = (TextView)findViewById(R.id.welcome_username);
		TextView userSalutation = (TextView)findViewById(R.id.welcome_salutation);
		userName.setText(username);
		
		Calendar c = Calendar.getInstance();
		int hours = c.get(Calendar.HOUR_OF_DAY);
		if(hours <= 12 )
			userSalutation.setText("Good Morning");
		else if(hours <= 17)
			userSalutation.setText("Good Afternoon");
		else
			userSalutation.setText("Good Evening");
		
		new Asynconnection2().execute();
	}
	
	public class Asynconnection2 extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generatLog.i("Time",String.valueOf(hours));ed method stub
			UserFunctions users = new UserFunctions();
			jObj = users.allZodiacs();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.i("ZODIAC", jObj.toString());
			
		}
	}
}
	


